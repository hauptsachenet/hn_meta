<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 21.11.16
 * Time: 10:11
 */

namespace Hn\HnMeta\Hook;


class RealurlAutoconf
{
    public function add($params)
    {
        $params['config']['fileName']['index']['robots.txt'] = [
            'keyValues' => ['type' => 114111098],
        ];
        $params['config']['fileName']['index']['browserconfig.xml'] = [
            'keyValues' => ['type' => 98114111],
        ];
        $params['config']['fileName']['index']['manifest.json'] = [
            'keyValues' => ['type' => 109097110],
        ];
        $params['config']['fileName']['index']['sitemap.xml'] = [
            'keyValues' => ['type' => 115105116],
        ];
        
        // favicon.ico does not work for now but define it anyways
        $params['config']['fileName']['index']['favicon.ico'] = [
            'keyValues' => ['type' => 102097118],
        ];

        return $params['config'];
    }
}
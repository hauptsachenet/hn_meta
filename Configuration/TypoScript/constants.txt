page {
    # cat=page; type=string; label=theme color (eg. for browser color on android and microsoft tile color, something like bootstraps @brand-primary)
    theme-color =
    # cat=page; type=string; label=favicon (absolute path)
    favicon = typo3conf/ext/hn_meta/Resources/Public/demo_favicon.png
    # cat=page; type=string; label=favicon background color (eg. for splash screen and ios icon background)
    favicon-background = #ffffff
    # cat=page; type=string; label=exclude canonical (comma separated; cHash will cause problems so exclude cHash for every param here)
    excludeCanonical = no_cache
    # cat=page; type=string; label=default html language tag (en or de; also possible is en-US but prefer it without region)
    defaultHtmlLanguage = en
    # cat=page; type=string; label=Id for tag manager
    googleTagManagerId =
}
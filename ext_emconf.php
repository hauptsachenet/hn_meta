<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'General Page Metadata',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Marco Pfeiffer',
    'author_email' => 'marco@hauptsache.net',
    'state' => 'stable',
    'internal' => '',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-8.7.999',
            'realurl' => '2.1',
        ],
    ],
];

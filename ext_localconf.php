<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}


// RealURL auto configuration hook
$autoconf = 'ext/realurl/class.tx_realurl_autoconfgen.php';
$autoconfPath = 'EXT:hn_meta/Classes/Hook/RealurlAutoconf.php:Hn\HnMeta\Hook\RealurlAutoconf->add';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][$autoconf]['extensionConfiguration']['hn_meta'] = $autoconfPath;
